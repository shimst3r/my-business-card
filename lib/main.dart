import 'package:flutter/material.dart';

void main() {
  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Color(0xffEBBB8C),
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                maxRadius: 100.0,
                backgroundImage: AssetImage('images/avatar.png'),
              ),
              SizedBox(height: 10.0),
              Text(
                'Nils Müller',
                style: TextStyle(
                  color: Color(0xff312F31),
                  fontFamily: 'Press Start 2P',
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                'SOFTWARE ENGINEER',
                style: TextStyle(
                  color: Color(0xff312F31),
                  fontFamily: 'Fira Sans',
                  fontSize: 20.0,
                ),
              ),
              SizedBox(
                height: 25.0,
                width: 150.0,
                child: Divider(
                  color: Color(0xff312F31),
                ),
              ),
              Card(
                color: Colors.white,
                elevation: 10.0,
                margin: EdgeInsets.symmetric(
                  vertical: 10.0,
                  horizontal: 25.0,
                ),
                child: ListTile(
                  leading: Icon(
                    Icons.my_location,
                    color: Color(0xffCB9F77),
                  ),
                  title: Text(
                    'Bielefeld, Germany',
                    style: TextStyle(
                      color: Color(0xff312F31),
                      fontFamily: 'Fira Sans',
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
              Card(
                color: Colors.white,
                elevation: 10.0,
                margin: EdgeInsets.symmetric(
                  vertical: 10.0,
                  horizontal: 25.0,
                ),
                child: ListTile(
                  leading: Icon(
                    Icons.mail_outline,
                    color: Color(0xffCB9F77),
                  ),
                  title: Text(
                    'shimst3r@gmail.com',
                    style: TextStyle(
                      color: Color(0xff312F31),
                      fontFamily: 'Fira Sans',
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
              Card(
                color: Colors.white,
                elevation: 10.0,
                margin: EdgeInsets.symmetric(
                  vertical: 10.0,
                  horizontal: 25.0,
                ),
                child: ListTile(
                  leading: Icon(
                    Icons.web_asset,
                    color: Color(0xffCB9F77),
                  ),
                  title: Text(
                    'shimst3r.gitlab.io',
                    style: TextStyle(
                      color: Color(0xff312F31),
                      fontFamily: 'Fira Sans',
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
