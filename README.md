# my_business_card

My Business Card is a Multiplatform Flutter App that displays my professional information

![A screenshot taken from the app. It shows an avatar of Nils and his professional information](screenshot.png)

## Background

This is my second [Flutter](https://flutter.dev) project, implemented as one of the challenges during [The Complete 2020 Flutter Development Bootcamp with Dart](https://www.udemy.com/course/flutter-bootcamp-with-dart/).

## Credits

The avatar is taken from my [website](https://shimst3r.gitlab.io).

